import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(suppress=True)

# this script converts from the backward polynomial we get from MSA calibration to the forward polynomial
# there is no closed form solution for this so we approximate it using a least squares fit and then calcuate
# the resulting error
# the steps are as follows:
#
# 1. Generate a mesh of image coordinates (x, y)
# 2. for each (x,y) coordinate calculate the distance from the principal point (r)
# 3. for each r, calculate the theta value using the backwards polynomial
# 4. generate theta^i for for i in range(1,5), we ignore the leading 0 in the fw/bw polynomials
# 5. solve for the forward coefficeints using r and theta's
# 6. verify that forward and backward polynomials result in a correct round trip of theta -> r -> theta

# ev019 sen_bar_fc_190 msa cal
bw_poly = [0, 0.001459044690037263, 2.389967452918333e-08, -6.170211030077951e-11, 2.898810799268855e-13]
cx = 958.6348812986882
cy = 644.0067545915194
height = 1208
width = 1920

# this affect how dense the mesh is, 1 means every pixel, 2 means every other pixel, etc
# seems to have little effect on end result error
step = 1
############
## Step 1 ##
############
# generate mesh of image coordinates
image_coords_y = np.arange(0, height, step)
image_coords_x = np.arange(0, width, step)
image_coords_x, image_coords_y = np.meshgrid(image_coords_x, image_coords_y)

############
## Step 2 ##
############
# distance from principal point along xy axes
image_coords_x = image_coords_x - cx
image_coords_y = image_coords_y - cy

# total distance from principal point
image_coords_r = np.sqrt(image_coords_x**2 + image_coords_y**2)


############
## Step 3 ##
############
# repeat 5 times for 5th order polynomial
image_coords_coeff = np.stack([image_coords_r**i for i in range(1,5)], axis=-1)

# setup bw_poly to broadcast across image_coords_r
bw_poly_ = np.array(bw_poly[1:]).reshape(1, 1, 4)

# apply backwards transformation from image coordinates to theta
theta = np.sum(bw_poly_ * image_coords_coeff, axis=2)

############
## Step 4 ##
############

# generate theta^i for for i in range(5)
theta_coeff = np.stack([theta**i for i in range(1, 5)], axis=-1)

############
## Step 5 ##
############

# solve for the forward coefficeints using r and theta's
image_coords_r_flat = image_coords_r.reshape((-1, 1))
theta_coeff_flat = theta_coeff.reshape((-1, 4))
fw_poly = np.linalg.lstsq(theta_coeff_flat, image_coords_r_flat, rcond=None)[0]

############
## Step 6 ##
############

# calculate error resulting from round trip of theta -> r -> theta
bw_poly_ = np.array(bw_poly[1:]).reshape(-1,1)
image_coords_r_flat = np.squeeze(theta_coeff_flat @ fw_poly)
image_coords_r_flat = np.stack([image_coords_r_flat**i for i in range(1,5)], axis=-1)
theta_coeff_flat_model = image_coords_r_flat @ bw_poly_
error = theta_coeff_flat_model - theta.reshape(-1,1)
print(f"max error: {np.max(np.abs(error))} radians or {np.max(np.abs(error)) * 180 / np.pi} degrees")
print(f"mean error: {np.mean(np.abs(error))} radians or {np.mean(np.abs(error)) * 180 / np.pi} degrees")

# errors seem constant around a given radius
# plt.imshow(error.reshape(height, width))
# plt.show()